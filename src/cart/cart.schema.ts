import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CartDocument = Cart & Document;

@Schema({ timestamps: true })

export class Cart {
  @Prop({
    required: true,
  })
  newCartId: string;
  @Prop({
    required: true,
  })
  cartId: string;
  @Prop({
    required: true,
  })
  token: string;
  @Prop({
    required: true,
  })
  status: string;
}

export const CartSchema = SchemaFactory.createForClass(Cart);
