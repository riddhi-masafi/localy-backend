import { Controller, Get, Post, Patch, Request, Body, Param } from '@nestjs/common';
import { CartService } from './cart.service';
import { CartDto } from './cart.dto';

@Controller('cart')
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @Get('/:id')
  getCart(@Param('id') id: string) {
    console.log('CartController => getCart : cartId : ',id);
    return this.cartService.getCartById(id);
  }

  @Post()
  saveCartDetails(@Body() data: CartDto) {
    console.log('CartController => saveCartDetails : request object : ',data);
    return this.cartService.saveCartDetails(data);
  }

  @Patch('/:id')
  updateCartDetails(@Param('id') id: string, @Body() data) {
    console.log('CartController => updateCartDetails : cartId & data : ',id, data);
    return this.cartService.updateCartDetails(id, data);
  }
}
