import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cart, CartDocument } from './cart.schema';
import { CartDto } from './cart.dto';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class CartService {
  constructor(@InjectModel(Cart.name) private cartModel: Model<CartDocument>,    private http: HttpService
  ) {}
  
  async getCartById(id: string) {
    try {
      const cart = await this.cartModel.findById(id).exec();
      return cart ? {data: cart, message: 'cart found'} : {data: null, message: 'cart not found'};
    } catch (err) {
      console.error(err);
      return {data: null, message: 'cart not found'};
    }
  }

  async saveCartDetails(data: CartDto) {
    const response = await this.copyCart(data.cartId, data.token);
    console.log('CartService => saveCartDetails : Copy cart response : ',response);
    
    if (response && response?.data && response.data?.Id !== '') {
      data = {...data, newCartId: response.data?.Id}
      const saveCartData = new this.cartModel(data);
      
      // check if cart exists
      // const cart = await this.findCart({cartId: data.cartId});
      // console.log('CartService => saveCartDetails : check if cart data exist : ',cart);
      // if (cart) {
      //   // Update cart data since its already saved
      //   await this.updateCartData(data, cart.id);
      // }else{
        saveCartData.save();
      // }
      return {
        message: 'cart registered successfully.',
        status: 'success',
        data: saveCartData
      };
    }else{
      return {
        message: 'cart registeration failed.',
        status: 'failed',
        data: response?.data
      };
    }
  }

  async updateCartDetails(id: string, data: any) {
    // check if cart exists
    const saveCartData = new this.cartModel(data);
    const cart = await this.getCartById(id);
    console.log('CartService => updateCartDetails : check if cart data exist : ',cart);
    if (cart.data && cart.data !== null) {
      const updatedCart = await this.updateCartData(data, id);
      console.log('CartService => updateCartDetails : updated cart : ', saveCartData);
      return {
        message: 'cart updated successfully.',
        data: updatedCart
      };
    }else{
      return {
        message: 'cart not found.',
        data: {}
      };
    }
    
  }

  async findCart(filters: any) {
    return this.cartModel.findOne(filters).exec();
  }

  async updateCartData(data: CartDto, id: string) {
    console.log('comes in update cart : ', data);
    
    const cart = await this.cartModel.findByIdAndUpdate(
      id,
      data,
      { new: false },
    );
    return cart;
  }

  async copyCart(id: string, token: string) {
    try {
      const url = `${process.env.D365_BASEURL}cart/${id}/Copy?api-version=7.3`;
      const headers = {
        accept: 'application/json',
        'accept-language': 'en-US',
        authorization: `id_token ${token}`,
        'oun': '00000001',
        'prefer': 'return=representation'
      };
      const payload = {"targetCartType":2};
      const response = await this.http
        .post(url, payload, { headers })
        .toPromise();
        return response;
    } catch (err) {
      console.error(err);
      return {data: null, message: 'Error copying cart'};
    }
  }
}
