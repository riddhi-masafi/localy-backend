import { IsNotEmpty } from 'class-validator';

export class CartDto {
  @IsNotEmpty()
  cartId: string;

  @IsNotEmpty()
  newCartId: string;

  @IsNotEmpty()
  token: string;

  @IsNotEmpty()
  status: string;
}
