import { Controller, Get, Post, Patch, Request, Body, Param } from '@nestjs/common';
import { LogService } from './log.service';
import { LogDto } from './log.dto';

@Controller('log')
export class LogController {
  constructor(private readonly logService: LogService) {}

  @Post()
  saveLogDetails(@Body() data: LogDto) {
    console.log('LogController => saveLogDetails : request object : ',data);
    return this.logService.saveLogDetails(data);
  }

}
