import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Log, LogDocument } from './log.schema';
import { LogDto } from './log.dto';
import axios from 'axios';

@Injectable()
export class LogService {
  constructor(@InjectModel(Log.name) private logModel: Model<LogDocument>) {}

  async saveLogDetails(data: LogDto) {
      const saveLogData = new this.logModel(data);
      saveLogData.save();
      return {
        message: 'log registered successfully.',
        status: 'success',
        data: saveLogData
      };
    
  }
}
