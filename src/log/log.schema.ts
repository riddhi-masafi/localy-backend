import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LogDocument = Log & Document;

@Schema()
export class Log {
  @Prop({
    required: true,
  })
  type: String;
  @Prop({
    required: true,
  })
  name: String;
  @Prop({
    required: true,
    type: {},
  })
  data: String;
}

export const LogSchema = SchemaFactory.createForClass(Log);
