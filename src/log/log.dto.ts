import { IsNotEmpty } from 'class-validator';

export class LogDto {
  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  data: string;
}
