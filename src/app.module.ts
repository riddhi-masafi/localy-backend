import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CartModule } from './cart/cart.module';
import { LogModule } from './log/log.module';
import { CheckoutModule } from './checkout/checkout.module';
import { ConfigModule } from '@nestjs/config';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    MailerModule.forRoot({
      transport: {
        host: process.env.EMAIL_HOST,
        port: 587,
        secure: false,
        auth: {
          user: process.env.EMAIL_USERNAME,
          pass: process.env.EMAIL_PASSWORD,
        },
      },
    }),
    MongooseModule.forRoot('mongodb://localhost/localy'),
    CartModule,
    LogModule,
    CheckoutModule,
    // MongooseModule.forRoot('mongodb://localhost:27017', {
    //   dbName: 'localy',
    // }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
