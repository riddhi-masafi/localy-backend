import { IsNotEmpty } from 'class-validator';

export class CreateCheckoutDto {
  @IsNotEmpty()
  channelType: string;

  @IsNotEmpty()
  cartId: string;

  @IsNotEmpty()
  userAuthToken: string;

  @IsNotEmpty()
  paymentReference: string;

  @IsNotEmpty()
  cartData: string;

  @IsNotEmpty()
  shippingAddress: string;

  @IsNotEmpty()
  cartAttributes: string;
}
