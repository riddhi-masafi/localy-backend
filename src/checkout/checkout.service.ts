import { BadRequestException, Injectable, ForbiddenException } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Checkout, CheckoutDocument } from './checkout.schema';
import { CreateCheckoutDto } from './createCheckout.dto';
import { validate } from 'class-validator';
import { LogDocument, Log } from 'src/log/log.schema';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class CheckoutService {
  constructor(
    @InjectModel(Checkout.name) private checkoutModel: Model<CheckoutDocument>,
    @InjectModel(Log.name) private logModel: Model<LogDocument>,
    private readonly mailService: MailerService,
    private http: HttpService
    ) {}

  private userAuthToken: String = '';
  private cartId: String = '';

  async saveCheckoutDetails(data: CreateCheckoutDto) {
      try{
        const saveCheckoutData = new this.checkoutModel(data);
        const errors = await validate(data);
        if (errors.length > 0) {
          throw new BadRequestException('Validation failed');
        }
        // check if cart exists
        const cart = await this.findCart({cartId: data.cartId});
        console.log('CheckoutService => saveCheckoutDetails : check if cart data exist : ', cart);
        if (cart) {
          // Update cart data since its already saved 24+5=29
          return await this.updateCartData(data, cart.id);
        }else{
          return saveCheckoutData.save();
        }
      } catch (err) {
        return err
      }
  }

  async findCart(filters: any) {
    return this.checkoutModel.findOne(filters).exec();
  }

  async updateCartDetails(id: string, data: any) {
    // check if cart exists
    const saveCartData = new this.checkoutModel(data);
    const cart = await this.getCartById(id);
    console.log('CheckoutService => updateCartDetails : check if cart data exist : ',cart);
    if (cart.data && cart.data !== null) {
      const updatedCart = await this.updateCartData(data, id);
      console.log('CheckoutService => updateCartDetails : updated cart : ', saveCartData);
      return {
        message: 'cart updated successfully.',
        data: updatedCart
      };
    }else{
      return {
        message: 'cart not found.',
        data: {}
      };
    }
  }

  async updateCartData(data: CreateCheckoutDto, id: string) {    
    return await this.checkoutModel.findByIdAndUpdate(
      id,
      data,
      { new: true },
    );
  }

  async getCartById(id: string) {
    try {
      const cart = await this.checkoutModel.findById(id).exec();
      return cart ? {data: cart, message: 'cart found'} : {data: null, message: 'cart not found'};
    } catch (err) {
      console.error(err);
      return {data: null, message: 'cart not found'};
    }
  }
  
  // Adyen webhook failure
  async onCancelOrder(data) {
    await this.saveLogData('cancelOrder', 'cancelOrderRequestObject', data);
  }

  // Adyen webhook success
  async onPlaceOrderCC(data: any) {
    // await this.saveLogData('placeOrder', 'placeOrderRequestObject', data);
    const checkoutLogData = await this.findCart({paymentReference: '666996cebe0e3b721cb52ddf'}); //data.cartId});
    if(checkoutLogData){
      return await this.processOrder(checkoutLogData);
    }else{
      return {data: null, message: 'cart details not found'};
    }
  }

  // Place order COD
  async onPlaceOrderCOD(id: string) {
    const checkoutLogData = await this.getCartById(id);
    if(checkoutLogData && checkoutLogData.data){
      return await this.processOrder(checkoutLogData.data);
    }else{
      return {status: 'failed', message: 'Checkout log details not found from DB', data: null};
    }
  }

  async processOrder(checkoutLogData: Checkout) {
    // console.log('CheckoutService => processOrder : checkout data logs : ', checkoutLogData);
    this.userAuthToken = checkoutLogData.userAuthToken;
    this.cartId = checkoutLogData.cartId;

    // Fetch cart details from MS
    const cartDetails = await this.fetchCartDetailsApi();

    if(cartDetails && cartDetails?.data && cartDetails?.data?.Id && cartDetails?.data?.Id !== '') {
      
      // Check if attributes are updated
      if(checkoutLogData.cartAttributes && checkoutLogData.cartAttributes?.length > 0){
        await this.updateCartAttributesApi(checkoutLogData.cartAttributes);
      }

      // Place order
      return await this.placeOrderApi(cartDetails.data, checkoutLogData);
    } else {
      return {status: 'failed', message: 'cart details not found from MS', data: null};
    }
  }

  // Fetch cart api
  async fetchCartDetailsApi() {
    try {
      const url = `${process.env.D365_BASEURL}cart/${this.cartId}?api-version=7.3`;
      const headers = {
        accept: 'application/json',
        'accept-language': 'en-US',
        authorization: `id_token ${this.userAuthToken}`,
        'oun': '00000001',
        'prefer': 'return=representation'
      };
      const response = await this.http
        .get(url, { headers })
        .toPromise();

        console.log('CheckoutService => fetchCartDetailsApi : response : ', response?.data);
        return response;
    } catch (err) {
      console.error('Error fetching cart data', err);
      return {data: null, message: 'Error fetching cart data'};
    }
  }
  
  // Update cart attr api
  async updateCartAttributesApi(cartAttributes: any) {
    try {
      const url = `${process.env.D365_BASEURL}cart/${this.cartId}?api-version=7.3`;
      const headers = {
        accept: 'application/json',
        'accept-language': 'en-US',
        authorization: `id_token ${this.userAuthToken}`,
        'oun': '00000001',
        'prefer': 'return=representation'
      };
      const response = await this.http
        .patch(url, cartAttributes, { headers })
        .toPromise();
        return response;
    } catch (err) {
      console.error(err);
      return {data: null, message: 'Error copying cart'};
    }
  }

  // place order api
  async placeOrderApi(cartDetails: any, checkoutLogData: any) {
    try {
      let payload = {};
      if(checkoutLogData.paymentReference === 'COD'){
        payload = {
          "receiptEmail": checkoutLogData.customerEmail,
          "cartTenderLines": [
              {
                  "@odata.type": "#Microsoft.Dynamics.Commerce.Runtime.DataModel.CartTenderLine",
                  "Amount@odata.type": "#Decimal",
                  "Currency": "AED",
                  "TenderTypeId": "2",
                  "Amount": cartDetails?.TotalAmount,
                  "CustomerId": cartDetails?.CustomerId
              }
          ],
          "cartVersion": cartDetails?.Version
        };
      }else{
        // TO-DO :: update paid order payload
        payload = {
          "receiptEmail": checkoutLogData.customerEmail,
          "cartTenderLines": [
              {
                  "@odata.type": "#Microsoft.Dynamics.Commerce.Runtime.DataModel.CartTenderLine",
                  "Amount@odata.type": "#Decimal",
                  "Currency": "AED",
                  "TenderTypeId": "8",
                  "Amount": cartDetails?.TotalAmount,
                  "CustomerId": cartDetails?.CustomerId
              }
          ],
          "cartVersion": cartDetails?.Version
        };
      }
      console.log('CheckoutService => placeOrderApi : payload : ', payload);

      const url = `${process.env.D365_BASEURL}cart/${this.cartId}/Checkout?api-version=7.3`;
      const headers = {
        accept: 'application/json',
        'accept-language': 'en-US',
        authorization: `id_token ${this.userAuthToken}`,
        'oun': '00000001',
        'prefer': 'return=representation'
      };
      const response = await this.http
        .post(url, payload, { headers })
        .toPromise();

      console.log('CheckoutService => placeOrderApi : response : ', response?.data);

      if(response && response?.data && response?.data?.Id && response?.data?.Id !== ''){
        return {status: 'sucess', message: 'Order placed successfully', data: response.data};
      }else{
        // Place order failed
        const message = `Place order failed. Error details : ${JSON.stringify(response.data)}`;
        await this.sendEmail(message, 'Place order fail');
        return {status: 'failed', message: 'Error placing order', data: response.data};
      }
      
    } catch (err) {
      console.error(err);
      // Send failure error
      const message = `Place order failed. Error details : ${err}`;
      await this.sendEmail(message, 'Place order fail');
      return {status: 'failed', message: 'Error placing order', data: err};
    }
  }

  async saveLogData(name: string, type: string, data: {}) {
    let logData = {
      name: name,
      type: type,
      data: data
    }
    console.log('CheckoutService => saveLogData : payload : ', logData);
    const saveLogData = new this.logModel(logData);
    const response = await saveLogData.save();
    console.log('CheckoutService => saveLogData : response : ', response);
    return response;
  }

  async sendEmail(message: string, subject: string) {
    return this.mailService.sendMail({
      from: `Localy<${process.env.EMAIL_USERNAME}>`,
      to: 'riddhi.rathod@masafi.com',
      subject: subject,
      text: message,
    });
  }

  // Update delivery specs api
  async updateDeliverySpecificationApi(cartDetails: any) {
    try {
      // Create payload for update
      const cartItemsPayload =
      cartDetails && 
      cartDetails?.CartLines &&
      cartDetails?.CartLines.length > 0 &&
      cartDetails.CartLines.map((val: any) => {
          return {
            LineId: val.LineId,
            DeliverySpecification: {
              DeliveryModeId: cartDetails?.DeliveryMode,
              DeliveryPreferenceTypeValue: 1,
              DeliveryAddress: cartDetails?.ShippingAddress,
            },
          };
        });
      const payload = { lineDeliverySpecifications: cartItemsPayload };
      
      const url = `${process.env.D365_BASEURL}cart/${this.cartId}/UpdateLineDeliverySpecifications?api-version=7.3`;
      const headers = {
        accept: 'application/json',
        'accept-language': 'en-US',
        authorization: `id_token ${this.userAuthToken}`,
        'oun': '00000001',
        'prefer': 'return=representation'
      };
      const response = await this.http
        .post(url, payload, { headers })
        .toPromise();
        return response;
    } catch (err) {
      console.error(err);
      return {data: null, message: 'Error copying cart'};
    }
  }
}
