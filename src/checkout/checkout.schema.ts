import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CheckoutDocument = Checkout & Document;

@Schema({ collection: 'checkoutData', timestamps: true })
export class Checkout {
  @Prop({
    required: true,
  })
  channelType: String;
  @Prop({
    // required: true,
  })
  customerId: String;

  @Prop({
    // required: true,
  })
  customerEmail: String;

  @Prop({
    // required: true,
  })
  couponCode: String;

  @Prop({
    required: true,
  })
  cartId: String;

  @Prop({
    required: true,
  })
  userAuthToken: String;

  @Prop({
    required: true,
  })
  paymentReference: String;

  @Prop({
    type: {}, required: true,
  })
  cartData: String;

  @Prop({
    type: {}, required: true,
  })
  shippingAddress: String;

  @Prop({
    type: {}, required: true,
  })
  cartAttributes: String;

  @Prop({
    required: false,
  })
  salesOrderReferenceId: String;
}

export const CheckoutSchema = SchemaFactory.createForClass(Checkout);
