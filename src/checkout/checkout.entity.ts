export class Checkout {
  channelType: string;
  cartId: string;
  userAuthToken: string;
  paymentReference: string;
  cartData: string;
  shippingAddress: string;
  cartAttributes: string;
  salesOrderReferenceId: string;
}
