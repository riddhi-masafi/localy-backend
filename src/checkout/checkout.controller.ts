import { Controller, Get, Post, Patch, Request, Body, Param } from '@nestjs/common';
import { CheckoutService } from './checkout.service';
import { CreateCheckoutDto } from './createCheckout.dto';

@Controller('checkout')
export class CheckoutController {
  constructor(private readonly checkoutService: CheckoutService) {}

  @Post('/data')
  saveCheckoutDetails(@Body() data: CreateCheckoutDto) {
    console.log('CheckoutController => saveCheckoutDetails : request object : ', data);
    return this.checkoutService.saveCheckoutDetails(data);
  }

  @Patch('/data/:id')
  updateCheckoutDetails(@Param('id') id: string, @Body() data: CreateCheckoutDto) {
    console.log('CheckoutController => updateCheckoutDetails : request object & id: ', data, id);
    return this.checkoutService.updateCartDetails(id, data);
  }

  @Get('/data/:id')
  getCart(@Param('id') id: string) {
    console.log('CartController => getCart : cartId : ', id);
    return this.checkoutService.getCartById(id);
  }

  @Post('/place-order')
  placeOrder(@Body() data: CreateCheckoutDto) {
    // console.log('CheckoutController => adyen webhook placeOrder : request object : ', data);
    return this.checkoutService.onPlaceOrderCC(data);
  }

  @Post('/process-order/:id')
  processOrder(@Param('id') id: string) {
    console.log('CheckoutController => processOrder cod : request id : ', id);
    return this.checkoutService.onPlaceOrderCOD(id);
  }

  @Post('/cancel-order')
  cancelOrder(@Body() data: CreateCheckoutDto) {
    console.log('CheckoutController => cancelOrder : request object : ', data);
    return this.checkoutService.onCancelOrder(data);
  }
}
